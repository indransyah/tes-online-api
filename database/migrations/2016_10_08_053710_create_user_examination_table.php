<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExaminationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_examination', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('examination_id');
            $table->float('score', 4, 2);
            $table->primary(['user_id', 'examination_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_examination');
    }
}
