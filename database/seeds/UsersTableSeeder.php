<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'indransyah',
            'email' => 'indransyah@gmail.com',
            'password' => app('hash')->make('indransyah'),
            'api_token' => str_random(32)
        ]);

        DB::table('users')->insert([
            'name' => 'mujib',
            'email' => 'mujibiqbal@gmail.com',
            'password' => app('hash')->make('mujib'),
            'api_token' => str_random(32)
        ]);

        DB::table('users')->insert([
            'name' => 'bintang',
            'email' => 'mabrurrohbintang@gmail.com',
            'password' => app('hash')->make('bintang'),
            'api_token' => str_random(32)
        ]);
    }
}