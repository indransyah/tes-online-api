<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at'
    ];


    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role');
    }

    public function assignRole($roleName)
    {
        if (is_string($roleName)) {
            $foundRole = Role::where('name', $roleName)->first();
            $this->roles()->attach($foundRole);
            return true;
        }
        return false;
    }

    public function revokeRole($roleName)
    {
        if (is_string($roleName)) {
            $foundRole = Role::where('name', $roleName)->first();
            $this->roles()->detach($foundRole);
            return true;
        }
        return false;
    }

    public function hasRole($roleName)
    {
        foreach ($this->roles as $role) {
            if ($role->name === $roleName)
                return true;
        }
        return false;
    }
}
