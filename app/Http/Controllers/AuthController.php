<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

// Model
use App\User;
use App\Role;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    /**
     * Signup user
     * @param  Request  $request    User request require input name, email, & password
     * @param  string   $roleName   Role name of the user
     * @return JSON     Success     200     {"message":"User successfully registered!"}
     *                  Error       400     {"message":"Cannot register the user!"}
     *                  Validation  422     {"email":["The email must be a valid email address.", ...], ...}
     */
    public function signup(Request $request, $roleName)
    {
        // Check role
        $isRoleExist = $this->checkRole($roleName);
        if (!$isRoleExist)
            return response('Not Found', 404);

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string'
        ]);

        DB::beginTransaction();
        $savedUser = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);
        $isUserAssigned = $savedUser->assignRole($roleName);
        if (!$savedUser || !$isUserAssigned) {
            DB::rollBack();
            return response(['message' => 'Cannot register the user!'], 400);
        }
        DB::commit();
        return response(['message' => 'User successfully registered!']);

    }

    /**
     * @param  Request  $request    User request require email & password
     * @param  string   $roleName   Role name of the user
     * @return JSON     Success     200     {"id":4,"name":"User6","email":"user6@gmail.com","api_token":"5a24dd3df4160e0696f8ac7b15d65ba301ec7498","roles":[{"id":3,"name":"user","pivot":{"user_id":4,"role_id":3}}]}
     *                  Error       400     {"message":"ERROR MESSAGE"}
     *                  Validation  422     {"email":["The email must be a valid email address.", ...], ...}
     */
    public function signin(Request $request, $roleName)
    {
        // Check role
        $isRoleExist = $this->checkRole($roleName);
        if (!$isRoleExist)
            return response('Not Found', 404);

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        $foundUser = User::where('email', $request->input('email'))->first();
        if (!$foundUser)
            return response(['message' => 'Email not registered!'], 400);

        $isUserPasswordCorrect = Hash::check($request->input('password'), $foundUser->password);
        if (!$isUserPasswordCorrect)
            return response(['message' => 'Wrong password!'], 400);
        
        $isUserHasRole = $foundUser->hasRole($roleName);
        if (!$isUserHasRole)
            return response('Unauthorized', 401);

        $foundUser->api_token = sha1(time());
        $isApiTokenUpdated = $foundUser->save();
        if (!$isApiTokenUpdated)
            return response(['message' => 'Cannot login!'], 400);
        return response($foundUser);
    }

    /**
     * @param  string   $roleName   Role name of the user  
     * @return Role     Founded one role
     */
    public function checkRole($roleName)
    {
        return Role::where('name', $roleName)->first();
    }
}
