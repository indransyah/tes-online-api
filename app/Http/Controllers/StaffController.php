<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    
    public function index(Request $request)
    {
        $user = $request->user();
        $res = [
            'success' => true,
            'message' => 'Staff',
            'data' => $user
        ];
        return response($res);
    }
}
