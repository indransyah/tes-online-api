<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roleName)
    {
        if ($request->input('api_token')) {
            $user = User::where('api_token', $request->input('api_token'))->first();
            if (!$user || !$user->hasRole($roleName)) return response('Unauthorized.', 401);
            return $next($request);
        }
        return response('Unauthorized.', 401);
    }
}
